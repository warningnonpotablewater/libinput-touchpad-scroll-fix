#define _GNU_SOURCE

#include <dlfcn.h>
#include <libinput.h>

const double LIBINPUT_SCROLL_FACTOR = 0.175;

typedef double (*get_axis_value_t)(
	struct libinput_event_pointer *event,
	enum libinput_pointer_axis axis
);

double libinput_event_pointer_get_axis_value (
	struct libinput_event_pointer *event,
	enum libinput_pointer_axis axis
) {
	get_axis_value_t get_axis_value = dlsym(RTLD_NEXT, "libinput_event_pointer_get_axis_value");
	
	return get_axis_value(event, axis) * LIBINPUT_SCROLL_FACTOR;
}
